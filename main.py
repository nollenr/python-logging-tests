import logging
import logging.config
import json
import pprint
import level1_1
import level1_2

class LogFilter(logging.Filter):
  def filter(self, record):
    print('\n\nprinting name: {}'.format(record.name))
    if record.name in ('level2_1'):
      return False
    else:
      return True

# instansiate the PP object
pp = pprint.PrettyPrinter(indent=4)

#https://gist.github.com/pmav99/49c01313db33f3453b22
with open("logging.json", "r") as fd:
  logging.config.dictConfig(json.load(fd))

logger = logging.getLogger(__name__)
logger.info('HI - I am logging from main')

level1_1.level1_1('DFSF')
level1_2.level1_2('ASDFSDF')
